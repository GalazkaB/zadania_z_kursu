package PizzaApp_enumy;

public enum Pizza {

    MARGHERITA (true, true, false, false),
    CAPRICIOSA (true, true, false, true),
    PROSCIUTTO (true, true, true, false);

    boolean cheese;
    boolean sauce;
    boolean ham ;
    boolean mushrooms;


    String description;

    private Pizza(boolean cheese, boolean sauce, boolean ham, boolean mushrooms){

        this.cheese = cheese;
        this.ham = ham;
        this.sauce = sauce;
        this.mushrooms = mushrooms;
    }


    public boolean getCheese(){
        return cheese;
    }

    public boolean getSauce(){
        return sauce;
     }

    public boolean getHam(){
        return ham;
     }

    public boolean getMushrooms(){
        return mushrooms;
     }


    @Override
    public String toString() {
        String result = this.name() +"(";
        if (cheese){
            result = result + "ser";
        }

        if (sauce){
            result = result + ", sos";
        }

        if(ham){
            result = result + ", szynka";
        }

        if(mushrooms){
            result = result + ", pieczarki";
        }

        return result +")";
    }




        /*Pizza pizza;
        switch (pizza){
            case Pizza.CAPRICIOSA:*/
    }





