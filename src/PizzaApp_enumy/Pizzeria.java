package PizzaApp_enumy;

import java.util.Scanner;

public class Pizzeria {

    public static void main(String[] args) {

        Pizzeria pizzeria = new Pizzeria();
        boolean exist = false;
        while (!exist) {
            System.out.println("W naszym menu: ");
            for (Pizza p : Pizza.values()) {

                System.out.println(p);
            }

            String order = pizzeria.takeOrder();
            String confirmation = pizzeria.orderConfirmation(order);
            if(confirmation!=null) {
                System.out.println(confirmation);
                exist = true;
            }else{
                System.out.println("Brak pizzy o podanej nazwie. Spróbuj jeszcze raz.");
            }
        }
    }


    public String takeOrder (){
        System.out.println("Jaką pizzę zamawiasz?");
        Scanner sc = new Scanner(System.in);
        String orderedPizza = sc.nextLine();
        return orderedPizza;


    }

    public String orderConfirmation (String p){
        for (Pizza pizza : Pizza.values()) {
            if (p.equals(pizza.name())) {
                String message = "Zamówiłeś pizzę: " + p+ " Dziękujemy!";
                return message;
            }
        }
        return null;
        }
    }

