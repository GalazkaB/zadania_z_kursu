import java.util.Scanner;
public class DecimalToHexadecimal {


    public static void main(String[] args) {
        char [] z = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        Scanner in = new Scanner(System.in);
        String hex = "";
        int deci = in.nextInt();
        int rem = 1;

        while(deci!=0){
            rem = deci%16;
            deci = (deci-rem)/16;
            hex =  z[rem]+hex;

     }

     System.out.println(hex);

    }
}
