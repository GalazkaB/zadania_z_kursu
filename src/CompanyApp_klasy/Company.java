package CompanyApp_klasy;

public class Company {
    public static void main (String []args){
        Employee e1 = new Employee();
        Employee e2 = new Employee();
        Employee e3 = new Employee();

        e1.birthDate = 1998;
        e2.birthDate = 1985;
        e3.birthDate = 1995;

        e1.name = "jgf";
        e2.name = "dfd";
        e3.name = "sdfs";

        e1.surname = "ss0";
        e2.surname = "fsdf";
        e3.surname = "sdfs";

        e1.workTime = 8;
        e2.workTime = 7;
        e3.workTime = 5;

        e1.wyswietlDane();
        e2.wyswietlDane();
        e3.wyswietlDane();
    }
}
