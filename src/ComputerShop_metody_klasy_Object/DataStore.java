package ComputerShop_metody_klasy_Object;

import java.util.ArrayList;

public class DataStore {
    public static final int MAX_POSITIONS = 10;
    int licznik = 0;
    ArrayList<Computer> cArray = new ArrayList<Computer>();

    public void add(Computer c) {
        if (licznik < MAX_POSITIONS){
            cArray.add(c);
        licznik++;
        }else{
            System.out.println("Nie można dodać pozycji. Przekroczono limit "+MAX_POSITIONS+" miejsc w bazie danych.");
        }
    }

    public void viewAll(){

        for (Computer i : cArray){
            System.out.println(i.toString());
        }
    }

    public int checkAvailability(Computer c){
        int count = 0;

        for (Computer i : cArray){
            if(i.equals(c))
                count++;
        }

        return count;

    }
}
