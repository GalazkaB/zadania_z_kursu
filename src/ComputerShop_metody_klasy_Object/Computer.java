package ComputerShop_metody_klasy_Object;

public class Computer {

    private String producer;
    private int model;

    public int getModel() {
        return model;
    }

    public String getProducer() {
        return producer;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Computer(String producer, int model) {
        setProducer(producer);
        setModel(model);
    }

    public boolean equals(Object c) {
        // sprawdzenie równości fizycznej za pomocą ==
        if (this == c)
            return true;
        // sprawdzamy, czy nie null
        if (c == null)
            return false;

        // sprawdzamy, czy przekazany argument jest typu Computer
        if (!(c instanceof Computer))
            return false;
        //rzutowanie na odpowiedni typ
        Computer com = (Computer) c;

        // sprawdzenie kolejnych pól klasy, uważając na wartości null
        if (this.producer == com.producer && this.model == com.model)
            return true;
        if (this.producer != null) {
            if (!this.producer.equals(com.producer))
                return false;
        } else if (this.producer == null && com.producer != null)
            return false;
        if (this.model != com.model)
            return false;

            //jeśli dojdziemy do tego miejsca to obiekty są równe
    return true;
    }



    @Override
    public String toString() {
        return producer + "," + model;
    }
}

