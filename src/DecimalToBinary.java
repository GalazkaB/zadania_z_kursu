import java.util.Scanner;
public class DecimalToBinary {
    public static void main (String [] args){

        Scanner in = new Scanner(System.in);
        char []b = {'0','1'};
        int deci = in.nextInt();
        String bin = "";
        int rem = 0;

        do {
            rem = deci%2;
            deci = deci/2;
            bin = b[rem]+bin;

        }while (deci != 0);
        System.out.println(bin);
    }
}
