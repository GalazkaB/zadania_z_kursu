package Hospital_polimorfizm;

public class HospitalApp {

    public static void main (String [] args){
        Hospital hospital = new Hospital(3);
        Doctor doctor1 = new Doctor("Zenon", "Zenonowicz", 4000, 555);
        Nurse nurse1 = new Nurse("Aneta", "Galazka", 2500, 400);
        Nurse nurse2 = new Nurse("Jolanta", "Jola", 2500, 500);
        hospital.addEmployee(doctor1);
        hospital.addEmployee(nurse1);
        hospital.addEmployee(nurse2);
        hospital.toString();
    }
}
