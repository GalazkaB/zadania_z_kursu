package Hospital_polimorfizm;

public class Hospital {
    public static int employeesCount = 0;

    int howManyEmployees;
    Person [] hospitalPeople;

    public Hospital(int howManyEmployees){
        this.howManyEmployees = howManyEmployees;
        hospitalPeople = new Person[howManyEmployees];
    }


    public void addEmployee(Person p){
        hospitalPeople[employeesCount]=p;
        employeesCount++;
    }

    public String toString(){
        System.out.println("Nasi pracownicy to: ");

        for(Person p : hospitalPeople){
            System.out.println(p.toString());
        }
        return null;
    }
}
