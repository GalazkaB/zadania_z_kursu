package Hospital_polimorfizm;

import javax.print.Doc;

public class Doctor extends Person {
    int bonus;

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public Doctor(String name, String surname, int salary, int bonus){
        super(name, surname, salary);
        setBonus(bonus);
    }

    public String toString(){
        return "Lekarz: "+name + " "+ surname+", którego pensja wynosi: "+salary
                +" zł i dostaje premię w wysokości: "+bonus+" zł";
    }
}
