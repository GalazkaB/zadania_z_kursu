package Hospital_polimorfizm;

public abstract class Person {
    String name;
    String surname;
    int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Person(String name, String surname, int salary) {
        setName(name);
        setSurname(surname);
        setSalary(salary);
    }
}