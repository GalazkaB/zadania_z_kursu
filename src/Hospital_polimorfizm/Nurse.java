package Hospital_polimorfizm;

public class Nurse extends Person {
    int overtime;

    public int getOvertime() {
        return overtime;
    }

    public void setOvertime(int overtime) {
        this.overtime = overtime;
    }

    public Nurse(String name, String surname, int salary, int overtime) {
        super(name, surname, salary);
        setOvertime(overtime);
    }

    public String toString() {
        return "Pielęgniarka: " + name + " " + surname + ", której pensja wynosi: " + salary
                + " zł i ma nadgodziny w ilości: " + overtime;
    }
}
